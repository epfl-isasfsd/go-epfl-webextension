# go.epfl.ch webextension

This webextension make life easier to shorten links on
[go.epfl.ch](https://go.epfl.ch) while browsing. Note that you have to be a
__member of the EPFL community__ to be able to create [go](https://go.epfl.ch)'s
short links and being able to create an API token in order to use this add-on.

<!-- TOC titleSize:2 tabSpaces:2 depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 skip:1 title:0 -->

- [Sample video](#sample-video)
- [Installation](#installation)
  - [Get the latest](#get-the-latest)
  - [Set the API token](#set-the-api-token)
  - [Shorten links!](#shorten-links)
- [Development](#development)
- [Contact](#contact)

<!-- /TOC -->
<!-- [![Watch the video](assets/webextension-example-preview.png)](https://gitlab.com/epfl-idevfsd/go-epfl-webextension/raw/master/assets/webextension-example.webm) -->

# Sample video

![Sample Video](assets/webextension-example.webm)


# Installation

In order to get this webextension running on your firefox, follow the next steps.


## Get the latest

This webextension is available for download as releases from this repo. Head to
the [releases
page](https://gitlab.com/epfl-idevfsd/go-epfl-webextension/-/releases), and
download the latest version.

From Firefox [about:addons](about:addons) page, follow the
[install add-on from file](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Distribution_options/Sideloading_add-ons#Using_Install_Add-on_From_File)
instructions.

Firefox will [request for permissions](https://support.mozilla.org/en-US/kb/permission-request-messages-firefox-extensions):
  * [Access your data for all websites](https://support.mozilla.org/en-US/kb/permission-request-messages-firefox-extensions?redirectlocale=en-US&redirectslug=permission-request-messages-explained#w_access-your-data-for-all-websites)
  * [Display notifications to you](https://support.mozilla.org/en-US/kb/permission-request-messages-firefox-extensions?redirectlocale=en-US&redirectslug=permission-request-messages-explained#w_display-notifications-to-you)
  * [Access browser tabs](https://support.mozilla.org/en-US/kb/permission-request-messages-firefox-extensions?redirectlocale=en-US&redirectslug=permission-request-messages-explained#w_access-browser-tabs)

![Accept the permissions](assets/permissions-request.png)

Once the required permissions accepted, you should see the add-on icon in the
Firefox toolbar.


## Set the API token

You need to authorize the extension to shorten links on go.epfl.ch on your
behalf. Go to the add-on preferences page (either from the add-on menu
(<kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>A</kbd>) or from the pop-up "info"
links), and open the preferences tab:

![go-webextension preferences](assets/go-webextension-preferences.png)

From here, head to your [go.epfl.ch](https://go.epfl.ch/profile) profile and
generate an API token.

![Generate API Token](assets/go-generate-api-token.png)

Copy the generated API Token and paste it in the add-on preferences.

Save the preferences and you're good to go!


## Shorten links!

Just use Firefox as you normally do. When you find yourself on a page you would
like to shorten the link, click on the extension icon (or use the shortcut
<kbd>ctrl</kbd>+<kbd>shift</kbd>+<kbd>U</kbd>), set the alias and shorten the
link.

![go.epfl.ch webextension pop-up](assets/go-webextension.png)


# Development

You probably know the drill: [Clone this
repo](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html), read
the <abbr title="addons.mozilla.org">AMO</abbr>
[documentation](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons), [test
your
work](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Temporary_Installation_in_Firefox),
[make a PR](https://docs.gitlab.com/ce/gitlab-basics/add-merge-request.html)...


# Contact

Feel free to email go-dev AT groupes dot epfl dot ch if you have request, or
create an [issue on this repo](https://gitlab.com/epfl-idevfsd/go-epfl-webextension/issues/new).
