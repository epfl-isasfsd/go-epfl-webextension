Changelog
=========

# v0.7
  - Auto-submit option (auto create link on randomize alias click)

# v0.6
  - Linter beautifullage
  - Better Makefile

# v0.5
  - FIX error message issue

# v0.4
  - Makefile and web-ext

# v0.3
  - API key options + preferences link
  - Better icons
  - Handle error messages
  - System notifications
  - Open popup shortcut

# v0.2
  - basic preferences
  - POST working

# v0.1
  - initial commit
