SHELL := /bin/bash
LATEST := $(shell cat manifest.json | jq -r .version)

all: check tag build

check: check-webext check-zip check-jq

check-webext:
	@type web-ext > /dev/null 2>&1 || { echo >&2 "Please install web-ext (npm install --global web-ext). Aborting."; exit 1; }

check-zip:
	@type zip > /dev/null 2>&1 || { echo >&2 "Please install zip. Aborting."; exit 1; }

check-jq:
	@type jq > /dev/null 2>&1 || { echo >&2 "Please install jq. Aborting."; exit 1; }

.PHONY: zip
zip: check-zip
	zip -r -FS go-epfl-extension.zip * \
	  --exclude *.git* \
		--exclude *.zip* \
		--exclude *.xpi \
		--exclude notes.md \
		--exclude Makefile \
		--exclude web-ext-artifacts\* \
		--exclude assets\* \
		--exclude .gitignore \
		--exclude .web-extension-id

.PHONY: build
build: check-webext zip
  # Need the WEB_EXT_API_KEY and WEB_EXT_API_SECRET as environment vars
	# https://addons.mozilla.org/en-US/developers/addon/api/key/
	web-ext sign -s `pwd` -i .git/* *.zip* *.xpi notes.md Makefile assets --api-key=$(WEB_EXT_API_KEY) --api-secret=$(WEB_EXT_API_SECRET)

.PHONY: version
tag: check-jq
	git status;
	git tag -a v$(LATEST) -m "Version $(LATEST)";
  #git show v$(LATEST);
	git push origin --tags;
