/*
Log that we received the message.
Then display a notification. The notification contains the URL,
which we read from the message.
*/

function notify (message) {
  console.log(message)
  browser.notifications.create({
    "type": "basic",
    "iconUrl": browser.extension.getURL("icons/link-48.png"),
    "title": "go.epfl.ch message",
    "message": message,
    "contextMessage": 'contextMessage'
  })
}

/*
Assign `notify()` as a listener to messages from the content script.
*/
browser.runtime.onMessage.addListener(notify)
