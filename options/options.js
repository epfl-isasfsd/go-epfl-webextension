function saveOptions (e) {
  e.preventDefault()
  browser.storage.sync.set({
    goToken: document.querySelector('#goToken').value,
    goRandom: document.querySelector('input#goRandom').checked,
    goAutoCopy: document.querySelector('input#goAutoCopy').checked
  })
  console.debug('# Preferences saved')
  console.debug(' - goToken:', document.querySelector('#goToken').value)
  console.debug(' - goRandom:', document.querySelector('input#goRandom').checked)
  console.debug(' - goAutoCopy:', document.querySelector('input#goAutoCopy').checked)
}

function restoreOptions () {
  function setAPIToken (result) {
    document.querySelector('#goToken').value = result.goToken || 'Please insert go API token'
  }

  function setRandOpt (result) {
    document.getElementById('goRandom').checked = result.goRandom
  }

  function setAutoCPOpt (result) {
    document.getElementById('goAutoCopy').checked = result.goAutoCopy
  }

  function onError (error) {
    console.log('Error:', error)
  }

  var tokenOpt = browser.storage.sync.get('goToken')
  tokenOpt.then(setAPIToken, onError)
  var randOpt = browser.storage.sync.get('goRandom')
  randOpt.then(setRandOpt, onError)
  var autoCPOpt = browser.storage.sync.get('goAutoCopy')
  autoCPOpt.then(setAutoCPOpt, onError)
}

document.addEventListener('DOMContentLoaded', restoreOptions)
document.querySelector('form').addEventListener('submit', saveOptions)

let version = browser.runtime.getManifest().version;
document.getElementById("version").innerHTML = '<br>Version ' + version;
